import '../styles/globals.css'
import {AuthProvider} from './context/UserAuthContext';
function MyApp({ Component, pageProps }) {
  return (
    <>
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    </>
  )
}

export default MyApp
