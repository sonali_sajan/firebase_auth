import React, { useState } from "react";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Divider from '@mui/material/Divider';
import GoogleIcon from '@mui/icons-material/Google';
import Link from '@mui/material/Link';
import { useUserAuth } from '../context/UserAuthContext';
import { useRouter } from "next/router";
import { FormControl } from '@mui/material';

const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);


const Login = () => {
  const router = useRouter()
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const { logIn, googleSignIn } = useUserAuth();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");
    try {
      await logIn(email, password);
      alert('login successfully')
      router.push('/component/Dashboard');
    } catch (e) {
      console.log(e.code)
      if(e.code=== 'auth/user-not-found'){
        alert('User Not Registered')
      }
      if(e.code=== 'auth/wrong-password'){
        alert('Wrong Password')
      }
      if(e.code=== 'auth/invalid-email'){
        alert('Enter Email Id')
      }
      if(e.code=== 'auth/internal-error'){
        alert('Enter Password')
      }
     
      setError(e.message);
    }
  };

  // const handleGoogleSignIn = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await googleSignIn();
  //     router.push("/component/Dashboard")
  //   } catch (error) {
  //     console.log(error.message);
  //   }
  // };



  return (
    <>
      <Card sx={{ minWidth: 50, textAlign: 'center' }}>
        <CardContent>
          <Typography varient="h2" sx={{ fontSize: 25, textAlign: 'center', fontWeight: 'bold', color: '#e91e63', p: 4 }} gutterBottom>
            LOGIN
          </Typography>
        </CardContent>
        <FormControl onSubmit={handleSubmit}>
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '100ch', textAlign: 'center' },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-basic"
            label="Email ID"
            variant="outlined"
            onChange={(e) => setEmail(e.target.value)}
            fullWidth
            autoComplete="off"
            />

          <TextField
            id="outlined-basic"
            type="password"
            label="Password"
            variant="outlined"
            onChange={(e) => setPassword(e.target.value)}
            fullWidth 
            autoComplete="off"
            />

          <Button
            variant="contained"
            fullWidth
            type="Submit"
            sx={{
              fontSize: 15,
              fontWeight: 'bold',
              color: '#fff',
              background: '#e91e63',
              '&:hover': {
                backgroundColor: '#ad1457',
              },
            }}
          >
            Login
          </Button>
          <Divider />
          {/* <Button variant="outlined" startIcon={<GoogleIcon />}
            onClick={handleGoogleSignIn}
          >
            Sign In With GOOGLE
          </Button> */}
          <CardContent>
            <Typography>Don't have an account? <Link href="/component/Register">Sign up</Link></Typography>
          </CardContent>
        </Box>
        </FormControl>
      </Card>
    </>
  )
}

export default Login