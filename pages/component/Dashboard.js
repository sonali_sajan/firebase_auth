
import Button from '@mui/material/Button';
import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import { useUserAuth } from "../context/UserAuthContext";
import { useRouter } from "next/router";
import Protected from "../component/Protected";
const Dashboard = () => {
    const router = useRouter()
    const { logOut, user } = useUserAuth();


    const handleLogout = async () => {
        try {
            await logOut();
            router.push("/component/Login")
        } catch (error) {
            console.log(error.message);
        }
    };

    return (
        <>
        <Protected>
            <Card sx={{ minWidth: 50, textAlign: 'center' }}>
                <CardContent>
                    <Typography varient="h2" sx={{ fontSize: 25, textAlign: 'center', fontWeight: 'bold', color: '#e91e63', p: 4 }} gutterBottom>
                        Hello!  Welcome
                        <br />
                        {user && user.email}
                    </Typography>
                </CardContent>
                <Button
                    variant="contained"
                    fullWidth
                    onClick={handleLogout}
                    sx={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        color: '#fff',
                        background: '#e91e63',
                        '&:hover': {
                            backgroundColor: '#ad1457',
                        }
                    }}
                >
                    Logout
                </Button>
            </Card>
            </Protected>
        </>
    )
}

export default Dashboard