import React from "react";
import { useUserAuth } from '../context/UserAuthContext';
import { useRouter } from "next/router";

const Protected = ({ children }) => {
    const router = useRouter()
  const { user } = useUserAuth();

  if (!user && !router.pathname.includes('login')) {
    router.push("/component/Login")
  }
  return children;
};

export default Protected;

