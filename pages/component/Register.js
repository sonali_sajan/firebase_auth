// import * as React from 'react';
import React, { useState } from "react";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Divider from '@mui/material/Divider';
import Link from '@mui/material/Link';
import { useUserAuth } from '../context/UserAuthContext';
import { useRouter } from "next/router";
import { FormControl } from '@mui/material';

const bull = (
    <Box
        component="span"
        sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
        •
    </Box>
);


const Register = () => {
    const router = useRouter()
    const [email, setEmail] = useState("");
    const [error, setError] = useState("");
    const [password, setPassword] = useState("");
    const { signUp } = useUserAuth();

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError("");
        try {
            await signUp(email, password);
            alert('Registration Successfully')
            router.push("/component/Dashboard")
        } catch (e) {
            console.log(e.code)
            
            if(e.code=== 'auth/email-already-in-use'){
                alert('Email Id Already In used')
            }
            if(e.code=== 'auth/invalid-password'){
                alert('Invalid Password')
            }
            if(e.code=== 'auth/internal-error'){
                alert('Invalid Error')
            }
            if(e.code=== 'auth/weak-password'){
                alert('Weak Password')
            }
            
            setError(e.message);
        }
    };  

    return (
        <>
            <Card sx={{ minWidth: 50, textAlign: 'center' }}>
                <CardContent>
                    <Typography varient="h2" sx={{ fontSize: 25, textAlign: 'center', fontWeight: 'bold', color: '#e91e63', p: 4 }} gutterBottom>
                        Register
                        <br />
                        {error.message}
                    </Typography>
                </CardContent>
                <FormControl onSubmit={handleSubmit}>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '100ch', textAlign: 'center' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField
                        id="outlined-basic"
                        label="Email ID"
                        variant="outlined"
                        onChange={(e) => setEmail(e.target.value)}
                        fullWidth
                        autoComplete="off"
                    />
                    <TextField
                        type="password"
                        id="outlined-basic"
                        label="Password"
                        variant="outlined"
                        onChange={(e) => setPassword(e.target.value)}
                        fullWidth
                        autoComplete="off"
                    />

                    <Button
                        variant="contained"
                        fullWidth
                        type="Submit"
                        sx={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            color: '#fff',
                            background: '#e91e63',
                            '&:hover': {
                                backgroundColor: '#ad1457',
                            },
                            // onSubmit: { handleSubmit }
                        }}
                    >
                        Register User
                    </Button>
                    <Divider />

                    <Button variant="outlined"
                    >
                        <Link href="/component/Login">Already have an Account</Link>
                    </Button>

                </Box>
                </FormControl>
            </Card>
        </>
    )
}

export default Register