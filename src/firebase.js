import { initializeApp } from "firebase/app";
import { getAuth} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyATy6LkfDJHBQyBpJ0Z2iSEFVy3lcsPO1k",
  authDomain: "user-f99d6.firebaseapp.com",
  projectId: "user-f99d6",
  storageBucket: "user-f99d6.appspot.com",
  messagingSenderId: "884526828196",
  appId: "1:884526828196:web:f00cb5756b4bb65e443431",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;
