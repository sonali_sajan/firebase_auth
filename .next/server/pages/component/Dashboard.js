"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/component/Dashboard";
exports.ids = ["pages/component/Dashboard"];
exports.modules = {

/***/ "./pages/component/Dashboard.js":
/*!**************************************!*\
  !*** ./pages/component/Dashboard.js ***!
  \**************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _mui_material_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @mui/material/Button */ \"@mui/material/Button\");\n/* harmony import */ var _mui_material_Button__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Button__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @mui/material/Box */ \"@mui/material/Box\");\n/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Box__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _mui_material_Card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @mui/material/Card */ \"@mui/material/Card\");\n/* harmony import */ var _mui_material_Card__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Card__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @mui/material/Typography */ \"@mui/material/Typography\");\n/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Typography__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _mui_material_CardContent__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @mui/material/CardContent */ \"@mui/material/CardContent\");\n/* harmony import */ var _mui_material_CardContent__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_mui_material_CardContent__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _context_UserAuthContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../context/UserAuthContext */ \"./pages/context/UserAuthContext.js\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _component_Protected__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../component/Protected */ \"./pages/component/Protected.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_7__, _component_Protected__WEBPACK_IMPORTED_MODULE_9__]);\n([_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_7__, _component_Protected__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\n\n\n\n\n\n\nconst Dashboard = ()=>{\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_8__.useRouter)();\n    const { logOut , user  } = (0,_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_7__.useUserAuth)();\n    const handleLogout = async ()=>{\n        try {\n            await logOut();\n            router.push(\"/component/Login\");\n        } catch (error) {\n            console.log(error.message);\n        }\n    };\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_component_Protected__WEBPACK_IMPORTED_MODULE_9__[\"default\"], {\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((_mui_material_Card__WEBPACK_IMPORTED_MODULE_4___default()), {\n                sx: {\n                    minWidth: 50,\n                    textAlign: \"center\"\n                },\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((_mui_material_CardContent__WEBPACK_IMPORTED_MODULE_6___default()), {\n                        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((_mui_material_Typography__WEBPACK_IMPORTED_MODULE_5___default()), {\n                            varient: \"h2\",\n                            sx: {\n                                fontSize: 25,\n                                textAlign: \"center\",\n                                fontWeight: \"bold\",\n                                color: \"#e91e63\",\n                                p: 4\n                            },\n                            gutterBottom: true,\n                            children: [\n                                \"Hello!  Welcome\",\n                                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n                                    fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n                                    lineNumber: 32,\n                                    columnNumber: 25\n                                }, undefined),\n                                user && user.email\n                            ]\n                        }, void 0, true, {\n                            fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n                            lineNumber: 30,\n                            columnNumber: 21\n                        }, undefined)\n                    }, void 0, false, {\n                        fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n                        lineNumber: 29,\n                        columnNumber: 17\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((_mui_material_Button__WEBPACK_IMPORTED_MODULE_1___default()), {\n                        variant: \"contained\",\n                        fullWidth: true,\n                        onClick: handleLogout,\n                        sx: {\n                            fontSize: 15,\n                            fontWeight: \"bold\",\n                            color: \"#fff\",\n                            background: \"#e91e63\",\n                            \"&:hover\": {\n                                backgroundColor: \"#ad1457\"\n                            }\n                        },\n                        children: \"Logout\"\n                    }, void 0, false, {\n                        fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n                        lineNumber: 36,\n                        columnNumber: 17\n                    }, undefined)\n                ]\n            }, void 0, true, {\n                fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n                lineNumber: 28,\n                columnNumber: 13\n            }, undefined)\n        }, void 0, false, {\n            fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\component\\\\Dashboard.js\",\n            lineNumber: 27,\n            columnNumber: 9\n        }, undefined)\n    }, void 0, false);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Dashboard);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9jb21wb25lbnQvRGFzaGJvYXJkLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUEwQztBQUNYO0FBQ0s7QUFDRTtBQUNZO0FBQ0U7QUFDSztBQUNqQjtBQUNPO0FBQy9DLE1BQU1TLFNBQVMsR0FBRyxJQUFNO0lBQ3BCLE1BQU1DLE1BQU0sR0FBR0gsc0RBQVMsRUFBRTtJQUMxQixNQUFNLEVBQUVJLE1BQU0sR0FBRUMsSUFBSSxHQUFFLEdBQUdOLHFFQUFXLEVBQUU7SUFHdEMsTUFBTU8sWUFBWSxHQUFHLFVBQVk7UUFDN0IsSUFBSTtZQUNBLE1BQU1GLE1BQU0sRUFBRSxDQUFDO1lBQ2ZELE1BQU0sQ0FBQ0ksSUFBSSxDQUFDLGtCQUFrQixDQUFDO1NBQ2xDLENBQUMsT0FBT0MsS0FBSyxFQUFFO1lBQ1pDLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDRixLQUFLLENBQUNHLE9BQU8sQ0FBQyxDQUFDO1NBQzlCO0tBQ0o7SUFFRCxxQkFDSTtrQkFDQSw0RUFBQ1YsNERBQVM7c0JBQ04sNEVBQUNMLDJEQUFJO2dCQUFDZ0IsRUFBRSxFQUFFO29CQUFFQyxRQUFRLEVBQUUsRUFBRTtvQkFBRUMsU0FBUyxFQUFFLFFBQVE7aUJBQUU7O2tDQUMzQyw4REFBQ2hCLGtFQUFXO2tDQUNSLDRFQUFDRCxpRUFBVTs0QkFBQ2tCLE9BQU8sRUFBQyxJQUFJOzRCQUFDSCxFQUFFLEVBQUU7Z0NBQUVJLFFBQVEsRUFBRSxFQUFFO2dDQUFFRixTQUFTLEVBQUUsUUFBUTtnQ0FBRUcsVUFBVSxFQUFFLE1BQU07Z0NBQUVDLEtBQUssRUFBRSxTQUFTO2dDQUFFQyxDQUFDLEVBQUUsQ0FBQzs2QkFBRTs0QkFBRUMsWUFBWTs7Z0NBQUMsaUJBRXpIOzhDQUFBLDhEQUFDQyxJQUFFOzs7OzZDQUFHO2dDQUNMaEIsSUFBSSxJQUFJQSxJQUFJLENBQUNpQixLQUFLOzs7Ozs7cUNBQ1Y7Ozs7O2lDQUNIO2tDQUNkLDhEQUFDN0IsNkRBQU07d0JBQ0g4QixPQUFPLEVBQUMsV0FBVzt3QkFDbkJDLFNBQVM7d0JBQ1RDLE9BQU8sRUFBRW5CLFlBQVk7d0JBQ3JCTSxFQUFFLEVBQUU7NEJBQ0FJLFFBQVEsRUFBRSxFQUFFOzRCQUNaQyxVQUFVLEVBQUUsTUFBTTs0QkFDbEJDLEtBQUssRUFBRSxNQUFNOzRCQUNiUSxVQUFVLEVBQUUsU0FBUzs0QkFDckIsU0FBUyxFQUFFO2dDQUNQQyxlQUFlLEVBQUUsU0FBUzs2QkFDN0I7eUJBQ0o7a0NBQ0osUUFFRDs7Ozs7aUNBQVM7Ozs7Ozt5QkFDTjs7Ozs7cUJBQ0s7cUJBQ2IsQ0FDTjtDQUNKO0FBRUQsaUVBQWV6QixTQUFTIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXV0aF91c2luZ19maXJlYmFzZV9hbmRfY29udGV4dF9hcGkvLi9wYWdlcy9jb21wb25lbnQvRGFzaGJvYXJkLmpzPzYxNTciXSwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCBCdXR0b24gZnJvbSAnQG11aS9tYXRlcmlhbC9CdXR0b24nO1xyXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBCb3ggZnJvbSAnQG11aS9tYXRlcmlhbC9Cb3gnO1xyXG5pbXBvcnQgQ2FyZCBmcm9tICdAbXVpL21hdGVyaWFsL0NhcmQnO1xyXG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdAbXVpL21hdGVyaWFsL1R5cG9ncmFwaHknO1xyXG5pbXBvcnQgQ2FyZENvbnRlbnQgZnJvbSAnQG11aS9tYXRlcmlhbC9DYXJkQ29udGVudCc7XHJcbmltcG9ydCB7IHVzZVVzZXJBdXRoIH0gZnJvbSBcIi4uL2NvbnRleHQvVXNlckF1dGhDb250ZXh0XCI7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgUHJvdGVjdGVkIGZyb20gXCIuLi9jb21wb25lbnQvUHJvdGVjdGVkXCI7XHJcbmNvbnN0IERhc2hib2FyZCA9ICgpID0+IHtcclxuICAgIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgICBjb25zdCB7IGxvZ091dCwgdXNlciB9ID0gdXNlVXNlckF1dGgoKTtcclxuXHJcblxyXG4gICAgY29uc3QgaGFuZGxlTG9nb3V0ID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IGxvZ091dCgpO1xyXG4gICAgICAgICAgICByb3V0ZXIucHVzaChcIi9jb21wb25lbnQvTG9naW5cIilcclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvci5tZXNzYWdlKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPD5cclxuICAgICAgICA8UHJvdGVjdGVkPlxyXG4gICAgICAgICAgICA8Q2FyZCBzeD17eyBtaW5XaWR0aDogNTAsIHRleHRBbGlnbjogJ2NlbnRlcicgfX0+XHJcbiAgICAgICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWVudD1cImgyXCIgc3g9e3sgZm9udFNpemU6IDI1LCB0ZXh0QWxpZ246ICdjZW50ZXInLCBmb250V2VpZ2h0OiAnYm9sZCcsIGNvbG9yOiAnI2U5MWU2MycsIHA6IDQgfX0gZ3V0dGVyQm90dG9tPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBIZWxsbyEgIFdlbGNvbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHt1c2VyICYmIHVzZXIuZW1haWx9XHJcbiAgICAgICAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxyXG4gICAgICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cclxuICAgICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICB2YXJpYW50PVwiY29udGFpbmVkXCJcclxuICAgICAgICAgICAgICAgICAgICBmdWxsV2lkdGhcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVMb2dvdXR9XHJcbiAgICAgICAgICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udFNpemU6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250V2VpZ2h0OiAnYm9sZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnI2ZmZicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICcjZTkxZTYzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJyY6aG92ZXInOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICcjYWQxNDU3JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgTG9nb3V0XHJcbiAgICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgICAgICA8L1Byb3RlY3RlZD5cclxuICAgICAgICA8Lz5cclxuICAgIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRGFzaGJvYXJkIl0sIm5hbWVzIjpbIkJ1dHRvbiIsIlJlYWN0IiwiQm94IiwiQ2FyZCIsIlR5cG9ncmFwaHkiLCJDYXJkQ29udGVudCIsInVzZVVzZXJBdXRoIiwidXNlUm91dGVyIiwiUHJvdGVjdGVkIiwiRGFzaGJvYXJkIiwicm91dGVyIiwibG9nT3V0IiwidXNlciIsImhhbmRsZUxvZ291dCIsInB1c2giLCJlcnJvciIsImNvbnNvbGUiLCJsb2ciLCJtZXNzYWdlIiwic3giLCJtaW5XaWR0aCIsInRleHRBbGlnbiIsInZhcmllbnQiLCJmb250U2l6ZSIsImZvbnRXZWlnaHQiLCJjb2xvciIsInAiLCJndXR0ZXJCb3R0b20iLCJiciIsImVtYWlsIiwidmFyaWFudCIsImZ1bGxXaWR0aCIsIm9uQ2xpY2siLCJiYWNrZ3JvdW5kIiwiYmFja2dyb3VuZENvbG9yIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/component/Dashboard.js\n");

/***/ }),

/***/ "./pages/component/Protected.js":
/*!**************************************!*\
  !*** ./pages/component/Protected.js ***!
  \**************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _context_UserAuthContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../context/UserAuthContext */ \"./pages/context/UserAuthContext.js\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_1__]);\n_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\nconst Protected = ({ children  })=>{\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();\n    const { user  } = (0,_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_1__.useUserAuth)();\n    if (!user && !router.pathname.includes(\"login\")) {\n        router.push(\"/component/Login\");\n    }\n    return children;\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Protected);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9jb21wb25lbnQvUHJvdGVjdGVkLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUEwQjtBQUMrQjtBQUNqQjtBQUV4QyxNQUFNRyxTQUFTLEdBQUcsQ0FBQyxFQUFFQyxRQUFRLEdBQUUsR0FBSztJQUNoQyxNQUFNQyxNQUFNLEdBQUdILHNEQUFTLEVBQUU7SUFDNUIsTUFBTSxFQUFFSSxJQUFJLEdBQUUsR0FBR0wscUVBQVcsRUFBRTtJQUU5QixJQUFJLENBQUNLLElBQUksSUFBSSxDQUFDRCxNQUFNLENBQUNFLFFBQVEsQ0FBQ0MsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1FBQy9DSCxNQUFNLENBQUNJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztLQUNoQztJQUNELE9BQU9MLFFBQVEsQ0FBQztDQUNqQjtBQUVELGlFQUFlRCxTQUFTLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9hdXRoX3VzaW5nX2ZpcmViYXNlX2FuZF9jb250ZXh0X2FwaS8uL3BhZ2VzL2NvbXBvbmVudC9Qcm90ZWN0ZWQuanM/MzhiMyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IHVzZVVzZXJBdXRoIH0gZnJvbSAnLi4vY29udGV4dC9Vc2VyQXV0aENvbnRleHQnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuXHJcbmNvbnN0IFByb3RlY3RlZCA9ICh7IGNoaWxkcmVuIH0pID0+IHtcclxuICAgIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgY29uc3QgeyB1c2VyIH0gPSB1c2VVc2VyQXV0aCgpO1xyXG5cclxuICBpZiAoIXVzZXIgJiYgIXJvdXRlci5wYXRobmFtZS5pbmNsdWRlcygnbG9naW4nKSkge1xyXG4gICAgcm91dGVyLnB1c2goXCIvY29tcG9uZW50L0xvZ2luXCIpXHJcbiAgfVxyXG4gIHJldHVybiBjaGlsZHJlbjtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFByb3RlY3RlZDtcclxuXHJcbiJdLCJuYW1lcyI6WyJSZWFjdCIsInVzZVVzZXJBdXRoIiwidXNlUm91dGVyIiwiUHJvdGVjdGVkIiwiY2hpbGRyZW4iLCJyb3V0ZXIiLCJ1c2VyIiwicGF0aG5hbWUiLCJpbmNsdWRlcyIsInB1c2giXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/component/Protected.js\n");

/***/ }),

/***/ "./pages/context/UserAuthContext.js":
/*!******************************************!*\
  !*** ./pages/context/UserAuthContext.js ***!
  \******************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"AuthProvider\": () => (/* binding */ AuthProvider),\n/* harmony export */   \"useUserAuth\": () => (/* binding */ useUserAuth)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/auth */ \"firebase/auth\");\n/* harmony import */ var _src_firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../src/firebase */ \"./src/firebase.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _src_firebase__WEBPACK_IMPORTED_MODULE_3__]);\n([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _src_firebase__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\nconst authContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)();\nfunction AuthProvider({ children  }) {\n    const { 0: user , 1: setUser  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({});\n    //auth is a firebase instant\n    function logIn(email, password) {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithEmailAndPassword)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, email, password);\n    }\n    function signUp(email, password) {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.createUserWithEmailAndPassword)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, email, password);\n    }\n    function logOut() {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signOut)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth);\n    }\n    function googleSignIn() {\n        const googleAuthProvider = new firebase_auth__WEBPACK_IMPORTED_MODULE_2__.GoogleAuthProvider();\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithPopup)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, googleAuthProvider);\n    }\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        const unsubscribe = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.onAuthStateChanged)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, (currentuser)=>{\n            console.log(\"Auth\", currentuser);\n            setUser(currentuser);\n        });\n        return ()=>{\n            unsubscribe();\n        };\n    }, []);\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(authContext.Provider, {\n        value: {\n            user,\n            logIn,\n            signUp,\n            logOut,\n            googleSignIn\n        },\n        children: children\n    }, void 0, false, {\n        fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\context\\\\UserAuthContext.js\",\n        lineNumber: 44,\n        columnNumber: 5\n    }, this);\n}\nfunction useUserAuth() {\n    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(authContext);\n}\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9jb250ZXh0L1VzZXJBdXRoQ29udGV4dC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBO0FBQXVFO0FBUWhEO0FBQ21CO0FBRTFDLE1BQU1XLFdBQVcsaUJBQUdYLG9EQUFhLEVBQUU7QUFFNUIsU0FBU1ksWUFBWSxDQUFDLEVBQUVDLFFBQVEsR0FBRSxFQUFFO0lBQ3pDLE1BQU0sRUFkUixHQWNTQyxJQUFJLEdBZGIsR0FjZUMsT0FBTyxNQUFJWiwrQ0FBUSxDQUFDLEVBQUUsQ0FBQztJQUVwQyw0QkFBNEI7SUFDNUIsU0FBU2EsS0FBSyxDQUFDQyxLQUFLLEVBQUVDLFFBQVEsRUFBRTtRQUM5QixPQUFPYix5RUFBMEIsQ0FBQ0ssK0NBQUksRUFBRU8sS0FBSyxFQUFFQyxRQUFRLENBQUMsQ0FBQztLQUMxRDtJQUNELFNBQVNDLE1BQU0sQ0FBQ0YsS0FBSyxFQUFFQyxRQUFRLEVBQUU7UUFDL0IsT0FBT2QsNkVBQThCLENBQUNNLCtDQUFJLEVBQUVPLEtBQUssRUFBRUMsUUFBUSxDQUFDLENBQUM7S0FDOUQ7SUFDRCxTQUFTRSxNQUFNLEdBQUc7UUFDaEIsT0FBT2Isc0RBQU8sQ0FBQ0csK0NBQUksQ0FBQyxDQUFDO0tBQ3RCO0lBQ0QsU0FBU1csWUFBWSxHQUFHO1FBQ3RCLE1BQU1DLGtCQUFrQixHQUFHLElBQUlkLDZEQUFrQixFQUFFO1FBQ25ELE9BQU9DLDhEQUFlLENBQUNDLCtDQUFJLEVBQUVZLGtCQUFrQixDQUFDLENBQUM7S0FDbEQ7SUFFRHBCLGdEQUFTLENBQUMsSUFBTTtRQUNkLE1BQU1xQixXQUFXLEdBQUdqQixpRUFBa0IsQ0FBQ0ksK0NBQUksRUFBRSxDQUFDYyxXQUFXLEdBQUs7WUFDNURDLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLE1BQU0sRUFBRUYsV0FBVyxDQUFDLENBQUM7WUFDakNULE9BQU8sQ0FBQ1MsV0FBVyxDQUFDLENBQUM7U0FDdEIsQ0FBQztRQUVGLE9BQU8sSUFBTTtZQUNYRCxXQUFXLEVBQUUsQ0FBQztTQUNmLENBQUM7S0FDSCxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRVAscUJBQ0UsOERBQUNaLFdBQVcsQ0FBQ2dCLFFBQVE7UUFDbkJDLEtBQUssRUFBRTtZQUFFZCxJQUFJO1lBQUVFLEtBQUs7WUFBRUcsTUFBTTtZQUFFQyxNQUFNO1lBQUVDLFlBQVk7U0FBRTtrQkFFbkRSLFFBQVE7Ozs7O1lBQ1ksQ0FDdkI7Q0FDSDtBQUVNLFNBQVNnQixXQUFXLEdBQUc7SUFDNUIsT0FBTzVCLGlEQUFVLENBQUNVLFdBQVcsQ0FBQyxDQUFDO0NBQ2hDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXV0aF91c2luZ19maXJlYmFzZV9hbmRfY29udGV4dF9hcGkvLi9wYWdlcy9jb250ZXh0L1VzZXJBdXRoQ29udGV4dC5qcz9lNTY1Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZUNvbnRleHQsIHVzZUNvbnRleHQsIHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7XG4gIGNyZWF0ZVVzZXJXaXRoRW1haWxBbmRQYXNzd29yZCxcbiAgc2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQsXG4gIG9uQXV0aFN0YXRlQ2hhbmdlZCxcbiAgc2lnbk91dCxcbiAgR29vZ2xlQXV0aFByb3ZpZGVyLFxuICBzaWduSW5XaXRoUG9wdXAsXG59IGZyb20gXCJmaXJlYmFzZS9hdXRoXCI7XG5pbXBvcnQgeyBhdXRoIH0gZnJvbSBcIi4uLy4uL3NyYy9maXJlYmFzZVwiO1xuXG5jb25zdCBhdXRoQ29udGV4dCA9IGNyZWF0ZUNvbnRleHQoKTtcblxuZXhwb3J0IGZ1bmN0aW9uIEF1dGhQcm92aWRlcih7IGNoaWxkcmVuIH0pIHtcbiAgY29uc3QgW3VzZXIsIHNldFVzZXJdID0gdXNlU3RhdGUoe30pO1xuXG4gIC8vYXV0aCBpcyBhIGZpcmViYXNlIGluc3RhbnRcbiAgZnVuY3Rpb24gbG9nSW4oZW1haWwsIHBhc3N3b3JkKSB7XG4gICAgcmV0dXJuIHNpZ25JbldpdGhFbWFpbEFuZFBhc3N3b3JkKGF1dGgsIGVtYWlsLCBwYXNzd29yZCk7XG4gIH1cbiAgZnVuY3Rpb24gc2lnblVwKGVtYWlsLCBwYXNzd29yZCkge1xuICAgIHJldHVybiBjcmVhdGVVc2VyV2l0aEVtYWlsQW5kUGFzc3dvcmQoYXV0aCwgZW1haWwsIHBhc3N3b3JkKTtcbiAgfVxuICBmdW5jdGlvbiBsb2dPdXQoKSB7XG4gICAgcmV0dXJuIHNpZ25PdXQoYXV0aCk7XG4gIH1cbiAgZnVuY3Rpb24gZ29vZ2xlU2lnbkluKCkge1xuICAgIGNvbnN0IGdvb2dsZUF1dGhQcm92aWRlciA9IG5ldyBHb29nbGVBdXRoUHJvdmlkZXIoKTtcbiAgICByZXR1cm4gc2lnbkluV2l0aFBvcHVwKGF1dGgsIGdvb2dsZUF1dGhQcm92aWRlcik7XG4gIH1cblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGNvbnN0IHVuc3Vic2NyaWJlID0gb25BdXRoU3RhdGVDaGFuZ2VkKGF1dGgsIChjdXJyZW50dXNlcikgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJBdXRoXCIsIGN1cnJlbnR1c2VyKTtcbiAgICAgIHNldFVzZXIoY3VycmVudHVzZXIpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIHVuc3Vic2NyaWJlKCk7XG4gICAgfTtcbiAgfSwgW10pO1xuXG4gIHJldHVybiAoXG4gICAgPGF1dGhDb250ZXh0LlByb3ZpZGVyXG4gICAgICB2YWx1ZT17eyB1c2VyLCBsb2dJbiwgc2lnblVwLCBsb2dPdXQsIGdvb2dsZVNpZ25JbiB9fVxuICAgID5cbiAgICAgIHtjaGlsZHJlbn1cbiAgICA8L2F1dGhDb250ZXh0LlByb3ZpZGVyPlxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdXNlVXNlckF1dGgoKSB7XG4gIHJldHVybiB1c2VDb250ZXh0KGF1dGhDb250ZXh0KTtcbn1cbiJdLCJuYW1lcyI6WyJjcmVhdGVDb250ZXh0IiwidXNlQ29udGV4dCIsInVzZUVmZmVjdCIsInVzZVN0YXRlIiwiY3JlYXRlVXNlcldpdGhFbWFpbEFuZFBhc3N3b3JkIiwic2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQiLCJvbkF1dGhTdGF0ZUNoYW5nZWQiLCJzaWduT3V0IiwiR29vZ2xlQXV0aFByb3ZpZGVyIiwic2lnbkluV2l0aFBvcHVwIiwiYXV0aCIsImF1dGhDb250ZXh0IiwiQXV0aFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJ1c2VyIiwic2V0VXNlciIsImxvZ0luIiwiZW1haWwiLCJwYXNzd29yZCIsInNpZ25VcCIsImxvZ091dCIsImdvb2dsZVNpZ25JbiIsImdvb2dsZUF1dGhQcm92aWRlciIsInVuc3Vic2NyaWJlIiwiY3VycmVudHVzZXIiLCJjb25zb2xlIiwibG9nIiwiUHJvdmlkZXIiLCJ2YWx1ZSIsInVzZVVzZXJBdXRoIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/context/UserAuthContext.js\n");

/***/ }),

/***/ "./src/firebase.js":
/*!*************************!*\
  !*** ./src/firebase.js ***!
  \*************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"auth\": () => (/* binding */ auth),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ \"firebase/app\");\n/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/auth */ \"firebase/auth\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_auth__WEBPACK_IMPORTED_MODULE_1__]);\n([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_auth__WEBPACK_IMPORTED_MODULE_1__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\nconst firebaseConfig = {\n    apiKey: \"AIzaSyATy6LkfDJHBQyBpJ0Z2iSEFVy3lcsPO1k\",\n    authDomain: \"user-f99d6.firebaseapp.com\",\n    projectId: \"user-f99d6\",\n    storageBucket: \"user-f99d6.appspot.com\",\n    messagingSenderId: \"884526828196\",\n    appId: \"1:884526828196:web:f00cb5756b4bb65e443431\"\n};\n// Initialize Firebase\nconst app = (0,firebase_app__WEBPACK_IMPORTED_MODULE_0__.initializeApp)(firebaseConfig);\nconst auth = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_1__.getAuth)(app);\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (app);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZmlyZWJhc2UuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUE2QztBQUNOO0FBRXZDLE1BQU1FLGNBQWMsR0FBRztJQUNyQkMsTUFBTSxFQUFFLHlDQUF5QztJQUNqREMsVUFBVSxFQUFFLDRCQUE0QjtJQUN4Q0MsU0FBUyxFQUFFLFlBQVk7SUFDdkJDLGFBQWEsRUFBRSx3QkFBd0I7SUFDdkNDLGlCQUFpQixFQUFFLGNBQWM7SUFDakNDLEtBQUssRUFBRSwyQ0FBMkM7Q0FDbkQ7QUFFRCxzQkFBc0I7QUFDdEIsTUFBTUMsR0FBRyxHQUFHVCwyREFBYSxDQUFDRSxjQUFjLENBQUM7QUFDbEMsTUFBTVEsSUFBSSxHQUFHVCxzREFBTyxDQUFDUSxHQUFHLENBQUMsQ0FBQztBQUNqQyxpRUFBZUEsR0FBRyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXV0aF91c2luZ19maXJlYmFzZV9hbmRfY29udGV4dF9hcGkvLi9zcmMvZmlyZWJhc2UuanM/NjdkOCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpbml0aWFsaXplQXBwIH0gZnJvbSBcImZpcmViYXNlL2FwcFwiO1xyXG5pbXBvcnQgeyBnZXRBdXRofSBmcm9tIFwiZmlyZWJhc2UvYXV0aFwiO1xyXG5cclxuY29uc3QgZmlyZWJhc2VDb25maWcgPSB7XHJcbiAgYXBpS2V5OiBcIkFJemFTeUFUeTZMa2ZESkhCUXlCcEowWjJpU0VGVnkzbGNzUE8xa1wiLFxyXG4gIGF1dGhEb21haW46IFwidXNlci1mOTlkNi5maXJlYmFzZWFwcC5jb21cIixcclxuICBwcm9qZWN0SWQ6IFwidXNlci1mOTlkNlwiLFxyXG4gIHN0b3JhZ2VCdWNrZXQ6IFwidXNlci1mOTlkNi5hcHBzcG90LmNvbVwiLFxyXG4gIG1lc3NhZ2luZ1NlbmRlcklkOiBcIjg4NDUyNjgyODE5NlwiLFxyXG4gIGFwcElkOiBcIjE6ODg0NTI2ODI4MTk2OndlYjpmMDBjYjU3NTZiNGJiNjVlNDQzNDMxXCIsXHJcbn07XHJcblxyXG4vLyBJbml0aWFsaXplIEZpcmViYXNlXHJcbmNvbnN0IGFwcCA9IGluaXRpYWxpemVBcHAoZmlyZWJhc2VDb25maWcpO1xyXG5leHBvcnQgY29uc3QgYXV0aCA9IGdldEF1dGgoYXBwKTtcclxuZXhwb3J0IGRlZmF1bHQgYXBwO1xyXG4iXSwibmFtZXMiOlsiaW5pdGlhbGl6ZUFwcCIsImdldEF1dGgiLCJmaXJlYmFzZUNvbmZpZyIsImFwaUtleSIsImF1dGhEb21haW4iLCJwcm9qZWN0SWQiLCJzdG9yYWdlQnVja2V0IiwibWVzc2FnaW5nU2VuZGVySWQiLCJhcHBJZCIsImFwcCIsImF1dGgiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/firebase.js\n");

/***/ }),

/***/ "@mui/material/Box":
/*!************************************!*\
  !*** external "@mui/material/Box" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("@mui/material/Box");

/***/ }),

/***/ "@mui/material/Button":
/*!***************************************!*\
  !*** external "@mui/material/Button" ***!
  \***************************************/
/***/ ((module) => {

module.exports = require("@mui/material/Button");

/***/ }),

/***/ "@mui/material/Card":
/*!*************************************!*\
  !*** external "@mui/material/Card" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("@mui/material/Card");

/***/ }),

/***/ "@mui/material/CardContent":
/*!********************************************!*\
  !*** external "@mui/material/CardContent" ***!
  \********************************************/
/***/ ((module) => {

module.exports = require("@mui/material/CardContent");

/***/ }),

/***/ "@mui/material/Typography":
/*!*******************************************!*\
  !*** external "@mui/material/Typography" ***!
  \*******************************************/
/***/ ((module) => {

module.exports = require("@mui/material/Typography");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/***/ ((module) => {

module.exports = import("firebase/app");;

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/***/ ((module) => {

module.exports = import("firebase/auth");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/component/Dashboard.js"));
module.exports = __webpack_exports__;

})();