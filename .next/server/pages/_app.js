/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _context_UserAuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./context/UserAuthContext */ \"./pages/context/UserAuthContext.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_2__]);\n_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_context_UserAuthContext__WEBPACK_IMPORTED_MODULE_2__.AuthProvider, {\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                ...pageProps\n            }, void 0, false, {\n                fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\_app.js\",\n                lineNumber: 7,\n                columnNumber: 9\n            }, this)\n        }, void 0, false, {\n            fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\_app.js\",\n            lineNumber: 6,\n            columnNumber: 7\n        }, this)\n    }, void 0, false);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBO0FBQThCO0FBQ3lCO0FBQ3ZELFNBQVNDLEtBQUssQ0FBQyxFQUFFQyxTQUFTLEdBQUVDLFNBQVMsR0FBRSxFQUFFO0lBQ3ZDLHFCQUNFO2tCQUNFLDRFQUFDSCxrRUFBWTtzQkFDWCw0RUFBQ0UsU0FBUztnQkFBRSxHQUFHQyxTQUFTOzs7OztvQkFBSTs7Ozs7Z0JBQ2Y7cUJBQ2QsQ0FDSjtDQUNGO0FBRUQsaUVBQWVGLEtBQUsiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9hdXRoX3VzaW5nX2ZpcmViYXNlX2FuZF9jb250ZXh0X2FwaS8uL3BhZ2VzL19hcHAuanM/ZTBhZCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJy4uL3N0eWxlcy9nbG9iYWxzLmNzcydcbmltcG9ydCB7QXV0aFByb3ZpZGVyfSBmcm9tICcuL2NvbnRleHQvVXNlckF1dGhDb250ZXh0JztcbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8QXV0aFByb3ZpZGVyPlxuICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XG4gICAgICA8L0F1dGhQcm92aWRlcj5cbiAgICA8Lz5cbiAgKVxufVxuXG5leHBvcnQgZGVmYXVsdCBNeUFwcFxuIl0sIm5hbWVzIjpbIkF1dGhQcm92aWRlciIsIk15QXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./pages/context/UserAuthContext.js":
/*!******************************************!*\
  !*** ./pages/context/UserAuthContext.js ***!
  \******************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"AuthProvider\": () => (/* binding */ AuthProvider),\n/* harmony export */   \"useUserAuth\": () => (/* binding */ useUserAuth)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/auth */ \"firebase/auth\");\n/* harmony import */ var _src_firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../src/firebase */ \"./src/firebase.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _src_firebase__WEBPACK_IMPORTED_MODULE_3__]);\n([firebase_auth__WEBPACK_IMPORTED_MODULE_2__, _src_firebase__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\nconst authContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)();\nfunction AuthProvider({ children  }) {\n    const { 0: user , 1: setUser  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({});\n    //auth is a firebase instant\n    function logIn(email, password) {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithEmailAndPassword)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, email, password);\n    }\n    function signUp(email, password) {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.createUserWithEmailAndPassword)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, email, password);\n    }\n    function logOut() {\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signOut)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth);\n    }\n    function googleSignIn() {\n        const googleAuthProvider = new firebase_auth__WEBPACK_IMPORTED_MODULE_2__.GoogleAuthProvider();\n        return (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.signInWithPopup)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, googleAuthProvider);\n    }\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        const unsubscribe = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.onAuthStateChanged)(_src_firebase__WEBPACK_IMPORTED_MODULE_3__.auth, (currentuser)=>{\n            console.log(\"Auth\", currentuser);\n            setUser(currentuser);\n        });\n        return ()=>{\n            unsubscribe();\n        };\n    }, []);\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(authContext.Provider, {\n        value: {\n            user,\n            logIn,\n            signUp,\n            logOut,\n            googleSignIn\n        },\n        children: children\n    }, void 0, false, {\n        fileName: \"E:\\\\Inprospect_Technologies\\\\Task_1\\\\firebase_auth(gitlab)\\\\pages\\\\context\\\\UserAuthContext.js\",\n        lineNumber: 44,\n        columnNumber: 5\n    }, this);\n}\nfunction useUserAuth() {\n    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(authContext);\n}\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9jb250ZXh0L1VzZXJBdXRoQ29udGV4dC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBO0FBQXVFO0FBUWhEO0FBQ21CO0FBRTFDLE1BQU1XLFdBQVcsaUJBQUdYLG9EQUFhLEVBQUU7QUFFNUIsU0FBU1ksWUFBWSxDQUFDLEVBQUVDLFFBQVEsR0FBRSxFQUFFO0lBQ3pDLE1BQU0sRUFkUixHQWNTQyxJQUFJLEdBZGIsR0FjZUMsT0FBTyxNQUFJWiwrQ0FBUSxDQUFDLEVBQUUsQ0FBQztJQUVwQyw0QkFBNEI7SUFDNUIsU0FBU2EsS0FBSyxDQUFDQyxLQUFLLEVBQUVDLFFBQVEsRUFBRTtRQUM5QixPQUFPYix5RUFBMEIsQ0FBQ0ssK0NBQUksRUFBRU8sS0FBSyxFQUFFQyxRQUFRLENBQUMsQ0FBQztLQUMxRDtJQUNELFNBQVNDLE1BQU0sQ0FBQ0YsS0FBSyxFQUFFQyxRQUFRLEVBQUU7UUFDL0IsT0FBT2QsNkVBQThCLENBQUNNLCtDQUFJLEVBQUVPLEtBQUssRUFBRUMsUUFBUSxDQUFDLENBQUM7S0FDOUQ7SUFDRCxTQUFTRSxNQUFNLEdBQUc7UUFDaEIsT0FBT2Isc0RBQU8sQ0FBQ0csK0NBQUksQ0FBQyxDQUFDO0tBQ3RCO0lBQ0QsU0FBU1csWUFBWSxHQUFHO1FBQ3RCLE1BQU1DLGtCQUFrQixHQUFHLElBQUlkLDZEQUFrQixFQUFFO1FBQ25ELE9BQU9DLDhEQUFlLENBQUNDLCtDQUFJLEVBQUVZLGtCQUFrQixDQUFDLENBQUM7S0FDbEQ7SUFFRHBCLGdEQUFTLENBQUMsSUFBTTtRQUNkLE1BQU1xQixXQUFXLEdBQUdqQixpRUFBa0IsQ0FBQ0ksK0NBQUksRUFBRSxDQUFDYyxXQUFXLEdBQUs7WUFDNURDLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLE1BQU0sRUFBRUYsV0FBVyxDQUFDLENBQUM7WUFDakNULE9BQU8sQ0FBQ1MsV0FBVyxDQUFDLENBQUM7U0FDdEIsQ0FBQztRQUVGLE9BQU8sSUFBTTtZQUNYRCxXQUFXLEVBQUUsQ0FBQztTQUNmLENBQUM7S0FDSCxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRVAscUJBQ0UsOERBQUNaLFdBQVcsQ0FBQ2dCLFFBQVE7UUFDbkJDLEtBQUssRUFBRTtZQUFFZCxJQUFJO1lBQUVFLEtBQUs7WUFBRUcsTUFBTTtZQUFFQyxNQUFNO1lBQUVDLFlBQVk7U0FBRTtrQkFFbkRSLFFBQVE7Ozs7O1lBQ1ksQ0FDdkI7Q0FDSDtBQUVNLFNBQVNnQixXQUFXLEdBQUc7SUFDNUIsT0FBTzVCLGlEQUFVLENBQUNVLFdBQVcsQ0FBQyxDQUFDO0NBQ2hDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXV0aF91c2luZ19maXJlYmFzZV9hbmRfY29udGV4dF9hcGkvLi9wYWdlcy9jb250ZXh0L1VzZXJBdXRoQ29udGV4dC5qcz9lNTY1Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZUNvbnRleHQsIHVzZUNvbnRleHQsIHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7XG4gIGNyZWF0ZVVzZXJXaXRoRW1haWxBbmRQYXNzd29yZCxcbiAgc2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQsXG4gIG9uQXV0aFN0YXRlQ2hhbmdlZCxcbiAgc2lnbk91dCxcbiAgR29vZ2xlQXV0aFByb3ZpZGVyLFxuICBzaWduSW5XaXRoUG9wdXAsXG59IGZyb20gXCJmaXJlYmFzZS9hdXRoXCI7XG5pbXBvcnQgeyBhdXRoIH0gZnJvbSBcIi4uLy4uL3NyYy9maXJlYmFzZVwiO1xuXG5jb25zdCBhdXRoQ29udGV4dCA9IGNyZWF0ZUNvbnRleHQoKTtcblxuZXhwb3J0IGZ1bmN0aW9uIEF1dGhQcm92aWRlcih7IGNoaWxkcmVuIH0pIHtcbiAgY29uc3QgW3VzZXIsIHNldFVzZXJdID0gdXNlU3RhdGUoe30pO1xuXG4gIC8vYXV0aCBpcyBhIGZpcmViYXNlIGluc3RhbnRcbiAgZnVuY3Rpb24gbG9nSW4oZW1haWwsIHBhc3N3b3JkKSB7XG4gICAgcmV0dXJuIHNpZ25JbldpdGhFbWFpbEFuZFBhc3N3b3JkKGF1dGgsIGVtYWlsLCBwYXNzd29yZCk7XG4gIH1cbiAgZnVuY3Rpb24gc2lnblVwKGVtYWlsLCBwYXNzd29yZCkge1xuICAgIHJldHVybiBjcmVhdGVVc2VyV2l0aEVtYWlsQW5kUGFzc3dvcmQoYXV0aCwgZW1haWwsIHBhc3N3b3JkKTtcbiAgfVxuICBmdW5jdGlvbiBsb2dPdXQoKSB7XG4gICAgcmV0dXJuIHNpZ25PdXQoYXV0aCk7XG4gIH1cbiAgZnVuY3Rpb24gZ29vZ2xlU2lnbkluKCkge1xuICAgIGNvbnN0IGdvb2dsZUF1dGhQcm92aWRlciA9IG5ldyBHb29nbGVBdXRoUHJvdmlkZXIoKTtcbiAgICByZXR1cm4gc2lnbkluV2l0aFBvcHVwKGF1dGgsIGdvb2dsZUF1dGhQcm92aWRlcik7XG4gIH1cblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGNvbnN0IHVuc3Vic2NyaWJlID0gb25BdXRoU3RhdGVDaGFuZ2VkKGF1dGgsIChjdXJyZW50dXNlcikgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJBdXRoXCIsIGN1cnJlbnR1c2VyKTtcbiAgICAgIHNldFVzZXIoY3VycmVudHVzZXIpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIHVuc3Vic2NyaWJlKCk7XG4gICAgfTtcbiAgfSwgW10pO1xuXG4gIHJldHVybiAoXG4gICAgPGF1dGhDb250ZXh0LlByb3ZpZGVyXG4gICAgICB2YWx1ZT17eyB1c2VyLCBsb2dJbiwgc2lnblVwLCBsb2dPdXQsIGdvb2dsZVNpZ25JbiB9fVxuICAgID5cbiAgICAgIHtjaGlsZHJlbn1cbiAgICA8L2F1dGhDb250ZXh0LlByb3ZpZGVyPlxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdXNlVXNlckF1dGgoKSB7XG4gIHJldHVybiB1c2VDb250ZXh0KGF1dGhDb250ZXh0KTtcbn1cbiJdLCJuYW1lcyI6WyJjcmVhdGVDb250ZXh0IiwidXNlQ29udGV4dCIsInVzZUVmZmVjdCIsInVzZVN0YXRlIiwiY3JlYXRlVXNlcldpdGhFbWFpbEFuZFBhc3N3b3JkIiwic2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQiLCJvbkF1dGhTdGF0ZUNoYW5nZWQiLCJzaWduT3V0IiwiR29vZ2xlQXV0aFByb3ZpZGVyIiwic2lnbkluV2l0aFBvcHVwIiwiYXV0aCIsImF1dGhDb250ZXh0IiwiQXV0aFByb3ZpZGVyIiwiY2hpbGRyZW4iLCJ1c2VyIiwic2V0VXNlciIsImxvZ0luIiwiZW1haWwiLCJwYXNzd29yZCIsInNpZ25VcCIsImxvZ091dCIsImdvb2dsZVNpZ25JbiIsImdvb2dsZUF1dGhQcm92aWRlciIsInVuc3Vic2NyaWJlIiwiY3VycmVudHVzZXIiLCJjb25zb2xlIiwibG9nIiwiUHJvdmlkZXIiLCJ2YWx1ZSIsInVzZVVzZXJBdXRoIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/context/UserAuthContext.js\n");

/***/ }),

/***/ "./src/firebase.js":
/*!*************************!*\
  !*** ./src/firebase.js ***!
  \*************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"auth\": () => (/* binding */ auth),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ \"firebase/app\");\n/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/auth */ \"firebase/auth\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_auth__WEBPACK_IMPORTED_MODULE_1__]);\n([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_auth__WEBPACK_IMPORTED_MODULE_1__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\nconst firebaseConfig = {\n    apiKey: \"AIzaSyATy6LkfDJHBQyBpJ0Z2iSEFVy3lcsPO1k\",\n    authDomain: \"user-f99d6.firebaseapp.com\",\n    projectId: \"user-f99d6\",\n    storageBucket: \"user-f99d6.appspot.com\",\n    messagingSenderId: \"884526828196\",\n    appId: \"1:884526828196:web:f00cb5756b4bb65e443431\"\n};\n// Initialize Firebase\nconst app = (0,firebase_app__WEBPACK_IMPORTED_MODULE_0__.initializeApp)(firebaseConfig);\nconst auth = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_1__.getAuth)(app);\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (app);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZmlyZWJhc2UuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUE2QztBQUNOO0FBRXZDLE1BQU1FLGNBQWMsR0FBRztJQUNyQkMsTUFBTSxFQUFFLHlDQUF5QztJQUNqREMsVUFBVSxFQUFFLDRCQUE0QjtJQUN4Q0MsU0FBUyxFQUFFLFlBQVk7SUFDdkJDLGFBQWEsRUFBRSx3QkFBd0I7SUFDdkNDLGlCQUFpQixFQUFFLGNBQWM7SUFDakNDLEtBQUssRUFBRSwyQ0FBMkM7Q0FDbkQ7QUFFRCxzQkFBc0I7QUFDdEIsTUFBTUMsR0FBRyxHQUFHVCwyREFBYSxDQUFDRSxjQUFjLENBQUM7QUFDbEMsTUFBTVEsSUFBSSxHQUFHVCxzREFBTyxDQUFDUSxHQUFHLENBQUMsQ0FBQztBQUNqQyxpRUFBZUEsR0FBRyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXV0aF91c2luZ19maXJlYmFzZV9hbmRfY29udGV4dF9hcGkvLi9zcmMvZmlyZWJhc2UuanM/NjdkOCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpbml0aWFsaXplQXBwIH0gZnJvbSBcImZpcmViYXNlL2FwcFwiO1xyXG5pbXBvcnQgeyBnZXRBdXRofSBmcm9tIFwiZmlyZWJhc2UvYXV0aFwiO1xyXG5cclxuY29uc3QgZmlyZWJhc2VDb25maWcgPSB7XHJcbiAgYXBpS2V5OiBcIkFJemFTeUFUeTZMa2ZESkhCUXlCcEowWjJpU0VGVnkzbGNzUE8xa1wiLFxyXG4gIGF1dGhEb21haW46IFwidXNlci1mOTlkNi5maXJlYmFzZWFwcC5jb21cIixcclxuICBwcm9qZWN0SWQ6IFwidXNlci1mOTlkNlwiLFxyXG4gIHN0b3JhZ2VCdWNrZXQ6IFwidXNlci1mOTlkNi5hcHBzcG90LmNvbVwiLFxyXG4gIG1lc3NhZ2luZ1NlbmRlcklkOiBcIjg4NDUyNjgyODE5NlwiLFxyXG4gIGFwcElkOiBcIjE6ODg0NTI2ODI4MTk2OndlYjpmMDBjYjU3NTZiNGJiNjVlNDQzNDMxXCIsXHJcbn07XHJcblxyXG4vLyBJbml0aWFsaXplIEZpcmViYXNlXHJcbmNvbnN0IGFwcCA9IGluaXRpYWxpemVBcHAoZmlyZWJhc2VDb25maWcpO1xyXG5leHBvcnQgY29uc3QgYXV0aCA9IGdldEF1dGgoYXBwKTtcclxuZXhwb3J0IGRlZmF1bHQgYXBwO1xyXG4iXSwibmFtZXMiOlsiaW5pdGlhbGl6ZUFwcCIsImdldEF1dGgiLCJmaXJlYmFzZUNvbmZpZyIsImFwaUtleSIsImF1dGhEb21haW4iLCJwcm9qZWN0SWQiLCJzdG9yYWdlQnVja2V0IiwibWVzc2FnaW5nU2VuZGVySWQiLCJhcHBJZCIsImFwcCIsImF1dGgiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/firebase.js\n");

/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = import("firebase/app");;

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/***/ ((module) => {

"use strict";
module.exports = import("firebase/auth");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();